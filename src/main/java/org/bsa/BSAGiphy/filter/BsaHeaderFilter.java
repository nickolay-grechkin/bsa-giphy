package org.bsa.BSAGiphy.filter;


import org.bsa.BSAGiphy.exception.NoBsaHeaderException;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class BsaHeaderFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse res = (HttpServletResponse) servletResponse;
        String bsaHeader = ((HttpServletRequest) servletRequest).getHeader("X-BSA-GIPHY");

        if (bsaHeader == null) {
            throw new NoBsaHeaderException();
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }
}
