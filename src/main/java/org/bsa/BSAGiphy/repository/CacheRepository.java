package org.bsa.BSAGiphy.repository;

import org.springframework.stereotype.Repository;

import java.io.File;
import java.util.*;

@Repository
public class CacheRepository {
    public Map<String, ArrayList<Map<String, ArrayList<String>>>> hashMap = new HashMap<>();
    static CacheRepository instance;

    private CacheRepository() {

    }

    public static CacheRepository getInstance() {
        if (instance == null) {
            instance = new CacheRepository();
            instance.seedData();
        }
        return instance;
    }

    private void seedData() {
        String[] usersList;
        String[] queriesList;
        String[] gifsPath;
        final File baseDirectory = new File("bsa_giphy\\users");
        usersList = getDirectories(baseDirectory);
        ArrayList<Map<String, ArrayList<String>>> queryList = new ArrayList<>();
        Map<String, ArrayList<String>> queryMap = new HashMap<>();
        ArrayList<String> arrayList1 = new ArrayList<>();
        for (String u : usersList) {
            queriesList = getDirectories(new File(baseDirectory + "\\" + u));
            for (String q : queriesList) {
                if (q.equals("history.csv")) {
                    continue;
                }
                gifsPath = getFilePaths(new File(baseDirectory + "\\" + u + "\\" + q));
                Collections.addAll(arrayList1, gifsPath);
                queryMap.put(q, arrayList1);
                arrayList1 = new ArrayList<>();
            }
            queryList.add(queryMap);
            hashMap.put(u, queryList);
            queryMap = new HashMap<>();
            queryList = new ArrayList<>();
        }
    }

    String[] getDirectories(File baseDirectory) {
        return baseDirectory.list();
    }

    String[] getFilePaths(File baseDirectory) {
        String[] arrayList = new String[Objects.requireNonNull(baseDirectory.listFiles()).length];
        for (int i = 0; i < Objects.requireNonNull(baseDirectory.listFiles()).length; i++) {
            arrayList[i] = Objects.requireNonNull(baseDirectory.listFiles())[i].getPath();
        }
        return arrayList;
    }

    public void addToRepository(String query, String userId, String path) {
        ArrayList<String> pathsArray = new ArrayList<>();
        pathsArray.add(path);
        ArrayList<Map<String, ArrayList<String>>> queryList = new ArrayList<>();
        Map<String, ArrayList<String>> queryMap = new HashMap<>();
        if (!hashMap.containsKey(userId)) {
            hashMap.put(userId, queryList);
            hashMap.get(userId).add(queryMap);
        }
        hashMap.get(userId).get(0).put(query, pathsArray);
    }

    public void removeFromRepository(String query, String userId) {
        if (query.trim().isEmpty()) {
            hashMap.remove(userId);
        } else {
            hashMap.get(userId).get(0).remove(query);
        }
    }

    public boolean isPresentInCache(String id, String query) {
        return hashMap.containsKey(id) && hashMap.get(id).get(0).containsKey(query);
    }
}
