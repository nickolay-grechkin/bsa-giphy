package org.bsa.BSAGiphy.controller;

import org.bsa.BSAGiphy.dto.GeneratedGifDto;
import org.bsa.BSAGiphy.service.GifService;
import org.bsa.BSAGiphy.service.UserService;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class ApiUserController {
    private final UserService userService;
    private final GifService gifService;
    Logger logger = LoggerFactory.getLogger(ApiUserController.class);

    @Autowired
    public ApiUserController(UserService userService, GifService gifService) {
        this.userService = userService;
        this.gifService = gifService;
    }

    @PostMapping("/{id}/generate")
    public ResponseEntity<Object> generateGif(@PathVariable String id, @RequestBody GeneratedGifDto generatedGifDto) throws JSONException {

        logger.info("POST request by USER: " + id + ", CLASS: ApiUserController, METHOD: generateGif(); QUERY: " +
                generatedGifDto.getQuery());
        if (validate(id)) {
            return new ResponseEntity<>("Incorrect id", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(userService.generateGif(id, generatedGifDto), HttpStatus.OK);
    }

    @GetMapping("/{id}/all")
    public ResponseEntity<Object> getGifsFromUserFolder(@PathVariable String id) {

        logger.info("GET request by USER: " + id + ", CLASS:ApiUserController , METHOD: getGifsFromUserFolder()");
        if (validate(id)) {
            return new ResponseEntity<>("Incorrect id", HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(userService.getGifsFromUserFolder(id), HttpStatus.OK);
    }

    @GetMapping("/{id}/search")
    public ResponseEntity<Object> searchGif(@PathVariable String id, @RequestParam String query, String force) {

        logger.info("GET request by USER: " + id + ", CLASS:ApiUserController , METHOD: searchGif(); QUERY: " +
                query + " " + "FORCE: " + force);
        String response = userService.findGif(id, query, force);
        if (response.trim().isEmpty()) {
            return new ResponseEntity<>("Gif not found", HttpStatus.NOT_FOUND);
        }
        else if (validate(id)) {
            return new ResponseEntity<>("Incorrect id", HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(userService.findGif(id, query, force), HttpStatus.OK);
    }

    @GetMapping("/{id}/history")
    public ResponseEntity<Object> getHistoryOfUser(@PathVariable String id) {

        logger.info("GET request by USER: " + id + ", CLASS:ApiUserController , METHOD: getHistoryOfUser()");
        if (validate(id)) {
            return new ResponseEntity<>("Incorrect id", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(userService.getHistoryOfUser(id), HttpStatus.OK);
    }

    @DeleteMapping("{id}/history/clean")
    public ResponseEntity<Object> deleteHistoryOfUser(@PathVariable String id) {

        logger.info("DELETE request by USER: " + id + ", CLASS:ApiUserController , METHOD: deleteHistoryOfUser()");
        if (validate(id)) {
            return new ResponseEntity<>("Incorrect id", HttpStatus.BAD_REQUEST);
        }
        userService.deleteHistoryOfUser(id);
        return new ResponseEntity<>("Deleted successful", HttpStatus.OK);
    }

    @DeleteMapping("{id}/reset")
    public ResponseEntity<Object> deleteMemoryCache(@PathVariable String id, @RequestParam String query) {

        logger.info("DELETE request by USER: " + id + ", CLASS:ApiUserController , METHOD: deleteMemoryCache(); QUERY: " +
                query);
        if (validate(id)) {
            return new ResponseEntity<>("Incorrect id", HttpStatus.BAD_REQUEST);
        }
        userService.deleteMemoryCache(id, query);

        return new ResponseEntity<Object>("Deleted successful", HttpStatus.OK);
    }

    @DeleteMapping("{id}/clean")
    public ResponseEntity<Object> deleteMemoryAndDiskCache(@PathVariable String id) {

        logger.info("DELETE request by USER: " + id + ", CLASS:ApiUserController , METHOD: deleteMemoryAndDiskCache()");
        if (validate(id)) {
            return new ResponseEntity<>("Incorrect id", HttpStatus.BAD_REQUEST);
        }
        userService.deleteMemoryAndDiskCache(id);
        return new ResponseEntity<>("Deleted successful", HttpStatus.OK);
    }

    public boolean validate (String userId) {
        return userId.trim().isEmpty() || !userId.matches("^[A-Za-z0-9]+$");
    }
}
