package org.bsa.BSAGiphy.controller;

import org.bsa.BSAGiphy.dto.CacheDto;
import org.bsa.BSAGiphy.dto.GeneratedGifDto;
import org.bsa.BSAGiphy.service.FileService;
import org.bsa.BSAGiphy.service.GifService;
import org.bsa.BSAGiphy.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping
public class ApiGifController {
    private final FileService fileService;
    private final UserService userService;
    private final GifService gifService;
    Logger logger = LoggerFactory.getLogger(ApiGifController.class);

    @Autowired
    public ApiGifController(FileService fileService, UserService userService, GifService gifService) {
        this.fileService = fileService;
        this.userService = userService;
        this.gifService = gifService;
    }


    @GetMapping("/gifs")
    public ArrayList<String> getAllGifs() {

        logger.info("GET request, CLASS:ApiGifController , METHOD: getAllGifs()");
        return fileService.getAllGifs();
    }

    @DeleteMapping("/cache")
    public void cleanCache() {

        logger.info("DELETE request, CLASS:ApiGifController , METHOD: cleanCache()");
        fileService.cleanCache("bsa_giphy\\cache");
    }

    @PostMapping("/cache/generate")
    public CacheDto generateGif(@RequestBody GeneratedGifDto generatedGifDto) {

        logger.info("POST request, CLASS:ApiGifController , METHOD: generateGif()");
        return userService.addGifToCache(generatedGifDto);
    }

    @GetMapping("/cache")
    public List<CacheDto> getCacheFromDisk(@RequestParam String query) {

        logger.info("GET request, CLASS:ApiGifController , METHOD: getCacheFromDisk()");
        return gifService.getCacheFromDisk(query);
    }
}


