package org.bsa.BSAGiphy.dto;

public class CacheDto {
    String query;
    String[] gifs;

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String[] getGifs() {
        return gifs;
    }

    public void setGifs(String[] gifs) {
        this.gifs = gifs;
    }
}
