package org.bsa.BSAGiphy.dto;

import java.io.Serializable;

public class GifFileDto implements Serializable {
    private Object data;

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
