package org.bsa.BSAGiphy.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class Handler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(NoBsaHeaderException.class)
    public ResponseEntity<Object> handleNoBsaHeaderException(NoBsaHeaderException noHeader) {
        return new ResponseEntity<>(noHeader, HttpStatus.FORBIDDEN);
    }
}
