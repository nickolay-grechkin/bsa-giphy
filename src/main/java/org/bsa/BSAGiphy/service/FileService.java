package org.bsa.BSAGiphy.service;

import org.bsa.BSAGiphy.dto.CacheDto;
import org.bsa.BSAGiphy.dto.GeneratedGifDto;
import org.bsa.BSAGiphy.dto.HistoryDto;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class FileService {
    public static final String PATH_TO_STORAGE = "bsa_giphy\\";

    public void saveGifToCache(String gifUrl, GeneratedGifDto gifDto) {
        try {
            Files.createDirectories(Path.of(PATH_TO_STORAGE + "cache\\" + gifDto.getQuery()));
            URL url = new URL(gifUrl);
            InputStream in = new BufferedInputStream(url.openStream());
            OutputStream out = new BufferedOutputStream(new FileOutputStream(PATH_TO_STORAGE + "cache\\" +
                    gifDto.getQuery() + "\\" + gifDto.getGifId() + ".gif"));
            for (int i; (i = in.read()) != -1; ) {
                out.write(i);
            }
            in.close();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String findGifReturnPath(String gifUrl, GeneratedGifDto gifDto) {
        String gifPath;
        Path path = Paths.get(PATH_TO_STORAGE + "cache\\" + gifDto.getQuery());
        if (gifDto.getForce() || !Files.exists(path)) {
            saveGifToCache(gifUrl, gifDto);
        }
        gifPath = transportGifToUserFolder(path, gifDto.getUserId(), gifDto.getQuery());
        saveToHistoryFile(gifDto.getQuery(), gifPath, gifDto.getUserId());
        return gifPath;
    }

    public String transportGifToUserFolder(Path path, String userId, String query) {
        File dir = new File(String.valueOf(path));
        File[] arrFiles = dir.listFiles();
        List<File> lst = Arrays.asList(arrFiles);
        Collections.shuffle(lst);
        Path personalPath = null;
        try {
            personalPath = Path.of(Files.createDirectories(Path.of("bsa_giphy\\users\\" + userId + "\\" +
                    query)) + "\\" + lst.get(0).getName());
            if(Files.exists(personalPath)) {
                return String.valueOf(personalPath);
            }
            Files.copy(Path.of(lst.get(0).getAbsolutePath()), personalPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return String.valueOf(personalPath);
    }

    public ArrayList<String> getAllGifs() {
        File baseDirectory = new File(String.valueOf(Path.of("bsa_giphy\\cache")));
        ArrayList<String> gifArray = new ArrayList<>();
        recursiveReadDirectory(baseDirectory, gifArray);
        return gifArray;
    }

    public static void recursiveReadDirectory(File baseDirectory, ArrayList<String> gifArray) {
        if (baseDirectory.isDirectory()) {
            for (File file : Objects.requireNonNull(baseDirectory.listFiles())) {
                if (file.isFile()) {
                    gifArray.add(file.getPath());
                } else {
                    recursiveReadDirectory(file, gifArray);
                }
            }
        }
    }

    public void cleanCache(String path) {
        File cacheFile = new File(path);
        for (File queryFile : Objects.requireNonNull(cacheFile.listFiles())) {
            File gifFile = new File(cacheFile + "\\" + queryFile.getName());
            if (queryFile.getName().equals("history.csv")) {
                queryFile.delete();
                continue;
            }
            for (File nf : Objects.requireNonNull(gifFile.listFiles())) {
                nf.delete();
            }
            queryFile.delete();
        }
    }

    public void createGifArray(File file, CacheDto cacheDto) {
        String[] arr = new String[Objects.requireNonNull(file.listFiles()).length];
        for (int i = 0; i < Objects.requireNonNull(file.listFiles()).length; i++) {
            try {
                arr[i] = Objects.requireNonNull(file.listFiles())[i].getCanonicalPath();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        cacheDto.setGifs(arr);
    }

    public List<CacheDto> getAllQueriesFromFolder(File file) {
        CacheDto cacheDto;
        List<CacheDto> list = new ArrayList<>();
        for (int i = 0; i < Objects.requireNonNull(file.list()).length; i++) {
            if (Objects.requireNonNull(file.list())[i].equals("history.csv")) {
                continue;
            }
            File queryFolder = new File(file + "\\" + Objects.requireNonNull(file.list())[i]);
            cacheDto = new CacheDto();
            createGifArray(queryFolder, cacheDto);
            cacheDto.setQuery(Objects.requireNonNull(file.list())[i]);
            list.add(cacheDto);
        }
        return list;
    }

    public void saveToHistoryFile(String query, String gifpath, String userId) {
        Path path = Path.of(PATH_TO_STORAGE + "users\\" + userId + "\\" + "history.csv");
        String timeStamp = new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime());
        StringBuilder builder = new StringBuilder();
        builder.append(timeStamp);
        builder.append(",");
        builder.append(query);
        builder.append(",");
        builder.append(gifpath);
        builder.append("\n");
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(String.valueOf(path), true))) {
            writer.write(builder.toString());

        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<HistoryDto> getUserHistory(String userId) {
        List<HistoryDto> list = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(PATH_TO_STORAGE + "users\\" + userId + "\\history.csv"));
            String line;
            Scanner scanner;
            int index = 0;

            while ((line = reader.readLine()) != null) {
                HistoryDto historyDto = new HistoryDto();
                scanner = new Scanner(line);
                scanner.useDelimiter(",");
                while (scanner.hasNext()) {
                    String data = scanner.next();
                    if (index == 0)
                        historyDto.setDate(data);
                    else if (index == 1)
                        historyDto.setQuery(data);
                    else if (index == 2)
                        historyDto.setGifPath(data);
                    index++;
                }
                index = 0;
                list.add(historyDto);
            }

            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

    public void deleteHistoryOfUser(String userId) {
        String path = PATH_TO_STORAGE + "users\\" + userId + "\\history.csv";
        File file = new File(path);
        file.delete();
        try {
            Files.createFile(Path.of(path));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
