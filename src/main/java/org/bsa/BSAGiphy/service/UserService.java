package org.bsa.BSAGiphy.service;

import org.bsa.BSAGiphy.dto.CacheDto;
import org.bsa.BSAGiphy.dto.GeneratedGifDto;
import org.bsa.BSAGiphy.dto.GifFileDto;
import org.bsa.BSAGiphy.dto.HistoryDto;
import org.bsa.BSAGiphy.repository.CacheRepository;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.json.JSONObject;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@Service
public class UserService {

    @Value("${giphy.url}")
    private String giphyUrl;

    @Value("${giphy.api.key}")
    private String giphyApiKey;
    FileService fileService = new FileService();

    public String generateGif(String id, GeneratedGifDto generatedGifDto) throws JSONException {
        generatedGifDto = createGifUrl(generatedGifDto);
        generatedGifDto.setUserId(id);

        return fileService.findGifReturnPath(generatedGifDto.getGifUrl(), generatedGifDto);
    }

    public GeneratedGifDto createGifUrl(GeneratedGifDto generatedGifDto) {
        RestTemplate restTemplate = new RestTemplate();
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(giphyUrl)
                .queryParam("api_key", giphyApiKey)
                .queryParam("tag", generatedGifDto.getQuery());
        GifFileDto gifFileDto = restTemplate.getForObject(builder.toUriString(), GifFileDto.class);
        JSONObject json = new JSONObject((Map) gifFileDto.getData());
        String gifId = null;
        try {
            gifId = json.getString("id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String gifUrl = "https://i.giphy.com/media/" + gifId + "/200.gif";
        generatedGifDto.setGifUrl(gifUrl);
        generatedGifDto.setGifId(gifId);
        return generatedGifDto;
    }

    public CacheDto addGifToCache(GeneratedGifDto generatedGifDto) {
        CacheDto cacheDto = new CacheDto();
        createGifUrl(generatedGifDto);
        fileService.saveGifToCache(generatedGifDto.getGifUrl(), generatedGifDto);
        cacheDto.setQuery(generatedGifDto.getQuery());
        File file = new File("bsa_giphy\\cache\\" + cacheDto.getQuery());
        fileService.createGifArray(file, cacheDto);
        return cacheDto;
    }

    public List<CacheDto> getGifsFromUserFolder(String id) {
        List<CacheDto> list;
        File userFolder = new File("bsa_giphy\\users\\" + id);
        list = fileService.getAllQueriesFromFolder(userFolder);
        return list;
    }

    public String findGif(String id, String query, String force) {
        ArrayList<String> arrayList;
        CacheRepository cacheRepository = CacheRepository.getInstance();
        if (cacheRepository.isPresentInCache(id, query) && force.trim().isEmpty()) {
            arrayList = cacheRepository.hashMap.get(id).get(0).get(query);
            Collections.shuffle(arrayList);
            return arrayList.get(0);
        } else {
            Path path = Paths.get("bsa_giphy\\" + "cache\\" + query);
            if (Files.exists(path)) {
                File file = new File(String.valueOf(path));
                File[] arrFiles = file.listFiles();
                List<File> lst = Arrays.asList(Objects.requireNonNull(arrFiles));
                Collections.shuffle(lst);
                if (!cacheRepository.isPresentInCache(id, query)) {
                    cacheRepository.addToRepository(query, id, lst.get(0).getPath());
                }
                return cacheRepository.hashMap.get(id).get(0).get(query).get(0);
            }
        }
        return " ";
    }

    public List<HistoryDto> getHistoryOfUser(String id) {
        return fileService.getUserHistory(id);
    }

    public void deleteHistoryOfUser(String id) {
        fileService.deleteHistoryOfUser(id);
    }

    public void deleteMemoryCache(String id, String query) {
        CacheRepository cacheRepository = CacheRepository.getInstance();
        System.out.println(cacheRepository.hashMap);
        cacheRepository.removeFromRepository(query, id);
        System.out.println(cacheRepository.hashMap);
    }

    public void deleteMemoryAndDiskCache(String id) {
        CacheRepository cacheRepository = CacheRepository.getInstance();
        fileService.cleanCache("bsa_giphy\\users\\" + id);
        cacheRepository.removeFromRepository("", id);
    }
}
