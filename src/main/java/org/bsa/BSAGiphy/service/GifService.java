package org.bsa.BSAGiphy.service;

import org.bsa.BSAGiphy.dto.CacheDto;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Service
public class GifService {
    FileService fileService = new FileService();

    public List<CacheDto> getCacheFromDisk(String query) {
        CacheDto cacheDto = new CacheDto();
        List<CacheDto> list = new ArrayList<>();
        if (query.trim().isEmpty()) {
            File cacheFolder = new File("bsa_giphy\\cache");
            list = fileService.getAllQueriesFromFolder(cacheFolder);
        } else {
            File file = new File("bsa_giphy\\cache\\" + query);
            fileService.createGifArray(file, cacheDto);
            cacheDto.setQuery(query);
            list.add(cacheDto);
        }
        return list;
    }
}
